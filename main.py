from Crypto.Cipher import AES
from Crypto.Util.Padding import pad
from base64 import b64encode
import base64
import os
import random
import string
import math
import zlib




alfa = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRCTUVWXYZ'
#генерация строки
def generate_random_string(length):
    return ''.join(random.choice(alfa) for i in range(length))
#генерация ключа
def keygen(n):
        return bytearray(random.getrandbits(8) for i in range(n))
#конвер байт в бит
def byte_to_bin(bytes):
    return ''.join(format(byte, '08b') for byte in bytes)

#конверт строки в bin
def str_to_bin(str):
    binary_converted = ''.join(format(ord(c), 'b') for c in str)
    return binary_converted

#случайное изменение бита в строке
def random_change_one_bit(string):
    length = len(string)
    pos = random.randint(0,length-1)
    if string[pos] == 0:
        r = string[:pos] + str(1) + string[pos+1:]
    else:
        r = string[:pos] + str(0) + string[pos+1:]
    return r
#шифрование aes eax mod
def encrypt(data, key):
    #key = str_to_bin(key)
    cipher = AES.new(key, AES.MODE_EAX)
    cipher_text= cipher.encrypt_and_digest(bytes(data.encode('utf-8')))
    res = [b64encode(x).decode('utf-8') for x in cipher_text]
    #res_tag = [b64encode(x).decode('utf-8') for x in tag]
    return res#[res, res_tag]
#шифрование aes cbc mod
def encrypt_CBC(data, key):
    cipher = AES.new(key, AES.MODE_CBC)
    ct_bytes = cipher.encrypt(pad(data.encode('utf-8'), AES.block_size))
    ct = b64encode(ct_bytes).decode('utf-8')
    return ct
#шифрование aes cfb mod
def encrypt_CFB(data, key):
    cipher = AES.new(key, AES.MODE_CFB)
    ct_bytes = cipher.encrypt(pad(data.encode('utf-8'), AES.block_size))
    ct = b64encode(ct_bytes).decode('utf-8')
    return ct
#шифрование aes ofb mod
def encrypt_OFB(data, key):
    cipher = AES.new(key, AES.MODE_OFB)
    ct_bytes = cipher.encrypt(pad(data.encode('utf-8'), AES.block_size))
    ct = b64encode(ct_bytes).decode('utf-8')
    return ct
#шифрование aes ecb mod
def encrypt_ECB(data, key):
    cipher = AES.new(key, AES.MODE_ECB)
    ct_bytes = cipher.encrypt(pad(data.encode('utf-8'), AES.block_size))
    ct = b64encode(ct_bytes).decode('utf-8')
    return ct
#добавление незначащих пробелов в строку
def add_null_to16(string):
    while (len(string)%16!=0):
        string += " "
    return string
#проверка ключа на длину
def check_key(string):
    if (len(string)%16!=0):
        return False
    else: 
        return True
#подсчет энтропии по шенону 
def count_shenon(string):
    sum = 0
    for i in string:
        match i:
            case "A"|"a":
                pi = 0.0796
                sum +=  math.log2(pi)*pi
            case "B"|"b":
                pi = 0.016
                sum +=  math.log2(pi)*pi
            case "C"|"c":
                pi = 0.0284
                sum +=  math.log2(pi)*pi
            case "D"|"d":
                pi = 0.0401
                sum +=  math.log2(pi)*pi
            case "E"|"e":
                pi = 0.1286
                sum +=  math.log2(pi)*pi
            case "F"|"f":
                pi = 0.0262
                sum +=  math.log2(pi)*pi
            case "G"|"g":
                pi = 0.0199
                sum +=  math.log2(pi)*pi
            case "H"|"h":
                pi = 0.0539
                sum +=  math.log2(pi)*pi
            case "I"|"i":
                pi = 0.0777
                sum +=  math.log2(pi)*pi
            case "J"|"j":
                pi = 0.0016
                sum +=  math.log2(pi)*pi
            case "K"|"k":
                pi = 0.0041
                sum +=  math.log2(pi)*pi
            case "L"|"l":
                pi = 0.0796
                sum +=  math.log2(pi)*pi
            case "M"|"m":
                pi = 0.0351
                sum +=  math.log2(pi)*pi
            case "N"|"n":
                pi = 0.0751
                sum +=  math.log2(pi)*pi
            case "O"|"o":
                pi = 0.0662
                sum +=  math.log2(pi)*pi
            case "P"|"p":
                pi = 0.0181
                sum +=  math.log2(pi)*pi
            case "Q"|"q":
                pi = 0.0017
                sum +=  math.log2(pi)*pi
            case "R"|"r":
                pi = 0.0683
                sum +=  math.log2(pi)*pi
            case "S"|"s":
                pi = 0.0662
                sum +=  math.log2(pi)*pi
            case "T"|"t":
                pi = 0.0972
                sum +=  math.log2(pi)*pi
            case "U"|"u":
                pi = 0.0248
                sum +=  math.log2(pi)*pi
            case "V"|"v":
                pi = 0.0115
                sum +=  math.log2(pi)*pi
            case "W"|"w":
                pi = 0.018
                sum +=  math.log2(pi)*pi
            case "X"|"x":
                pi = 0.0017
                sum +=  math.log2(pi)*pi
            case "Y"|"y":
                pi = 0.0152
                sum +=  math.log2(pi)*pi
            case "Z"|"z":
                pi = 0.0005
                sum +=  math.log2(pi)*pi
    sum = -sum
    return sum
#извлечение из строки всех символов, кроме символов английского алфавита
def ExtractAlphanumeric(InputString):
    return "".join([ch for ch in InputString if ch in (alfa)])


end = -1
while end != 0:
    try:
        os.system("cls")
        print("___AES cipher menu___")
        print("1. Random change bit in plain text and in key (autogen)")
        print("2. Random change bit in plain text and in key (manual)")
        print("3. Entropy in input text and cipher text")
        print("4. Compress")
        print("5. CBC mode")
        print("6. CFB mode")
        print("7. OFB mode")
        print("8. ECB mode")
        print("0. Exit")
        command = int(input("Input: "))
        match command:
            case 1:
                os.system("cls")
                string=generate_random_string(32)
                bit_string = str_to_bin(string)
                key = keygen(16)
                #print(key.decode('UTF-8'))
                print("сгенерированная строка: "+string)
                print("сгенерированный ключ (bit):   "+ str(byte_to_bin(key)))
                print("сгенерированный ключ (byte):   "+ str(repr(key)[12:-2]))
                print("")
                print("сгенерированная строка в виде бит: "+str(bit_string))
                print("строка с измененным битом:         "+str(random_change_one_bit(str_to_bin(string))))
                print("")
                cipher_text= encrypt(string, key)
                print("шифр текст:             "+str(cipher_text[0]))
                print("tag:                    "+str(cipher_text[1]))
                os.system("PAUSE")  
            case 2:
                os.system("cls")
                string = input("ведите входную строку: ")
                string = add_null_to16(string)
                key = input("введите ключ: ")
                #key = bytearray(key.encode())
                check = check_key(key)
                while (check_key(key)==False):
                    os.system("cls")
                    print("длина ключа должна быть кратна 16")
                    key = input("введите ключ: ")
                key = bytearray(key.encode())
                os.system("cls")

                bit_string = str_to_bin(string)
                
                print("входная строка: "+string)
                print("ключ (bit):    "+ str(byte_to_bin(key)))
                print("ключ (byte):   "+ str(repr(key)[12:-2]))
                print("")
                print("входная строка в виде бит: "+str(bit_string))
                print("строка с измененным битом:         "+str(random_change_one_bit(str_to_bin(string))))
                print("")
                cipher_text = encrypt(string, key)
                print("шифр текст:             "+str(cipher_text[0]))
                print("tag:                    "+str(cipher_text[1]))
                os.system("PAUSE")
            case 3:
                os.system("cls")
                string=generate_random_string(32)
                bit_string = str_to_bin(string)
                key = keygen(16)
                #print(key.decode('UTF-8'))
                print("сгенерированная строка: "+string)
                print("сгенерированный ключ (bit):   "+ str(byte_to_bin(key)))
                print("сгенерированный ключ (byte):   "+ str(repr(key)[12:-2]))
                print("")
                print("сгенерированная строка в виде бит: "+str(bit_string))
                print("строка с измененным битом:         "+str(random_change_one_bit(str_to_bin(string))))
                print("")
                cipher_text = encrypt(string, key)
                print("шифр текст:             "+str(cipher_text[0]))
                print("tag:                    "+str(cipher_text[1]))
                print("")
                print("энтропия открытого текста: "+str(count_shenon(string)))
                clean_str = ExtractAlphanumeric(cipher_text[0])
                print("энтропия шифртекста:       "+str(count_shenon(clean_str)))
                print()
                os.system("PAUSE")
            case 4:
                os.system("cls")
                num = input("ведите длину строки: ")
                string = generate_random_string(int(num))
                long_text_compressed = zlib.compress(string.encode('utf-8'))
                print("исходная строка: "+string)
                out = base64.b64encode(long_text_compressed)
                print("сжатый текст (bytes):    "+ str(repr(out)[2:-1]))
                print("коэффициент сжатия: "+str(len(string)/len(long_text_compressed)))
                os.system("PAUSE")
            case 5:
                os.system("cls")
                string=generate_random_string(32)
                bit_string = str_to_bin(string)
                key = keygen(16)
                #print(key.decode('UTF-8'))
                print("сгенерированная строка: "+string)
                print("сгенерированный ключ (bit):   "+ str(byte_to_bin(key)))
                print("сгенерированный ключ (byte):   "+ str(repr(key)[12:-2]))
                print("")
                print("сгенерированная строка в виде бит: "+str(bit_string))
                print("строка с измененным битом:         "+str(random_change_one_bit(str_to_bin(string))))
                print("")
                cipher_text= encrypt_CBC(string, key)
                print("шифр текст:               "+str(cipher_text))
                string+=generate_random_string(8)
                cipher_text_add_block= encrypt_CBC(string, key)
                print("шифр текст с блоком:      "+str(cipher_text_add_block))
                string=string[:-8]
                cipher_text_rm_block= encrypt_CBC(string, key)
                print("шифр текст без блока:     "+str(cipher_text_rm_block))
                string=string[:-8]
                string+=generate_random_string(8)
                cipher_text_ch_block= encrypt_CBC(string, key)
                print("шифр текст с изм. блоком: "+str(cipher_text_ch_block))
                os.system("PAUSE")  
            case 6:
                os.system("cls")
                string=generate_random_string(32)
                bit_string = str_to_bin(string)
                key = keygen(16)
                #print(key.decode('UTF-8'))
                print("сгенерированная строка: "+string)
                print("сгенерированный ключ (bit):   "+ str(byte_to_bin(key)))
                print("сгенерированный ключ (byte):   "+ str(repr(key)[12:-2]))
                print("")
                print("сгенерированная строка в виде бит: "+str(bit_string))
                print("строка с измененным битом:         "+str(random_change_one_bit(str_to_bin(string))))
                print("")
                cipher_text= encrypt_CFB(string, key)
                print("шифр текст:               "+str(cipher_text))
                string+=generate_random_string(8)
                cipher_text_add_block= encrypt_CFB(string, key)
                print("шифр текст с блоком:      "+str(cipher_text_add_block))
                string=string[:-8]
                cipher_text_rm_block= encrypt_CFB(string, key)
                print("шифр текст без блока:     "+str(cipher_text_rm_block))
                string=string[:-8]
                string+=generate_random_string(8)
                cipher_text_ch_block= encrypt_CFB(string, key)
                print("шифр текст с изм. блоком: "+str(cipher_text_ch_block))
                os.system("PAUSE")
            case 7:
                os.system("cls")
                string=generate_random_string(32)
                bit_string = str_to_bin(string)
                key = keygen(16)
                #print(key.decode('UTF-8'))
                print("сгенерированная строка: "+string)
                print("сгенерированный ключ (bit):   "+ str(byte_to_bin(key)))
                print("сгенерированный ключ (byte):   "+ str(repr(key)[12:-2]))
                print("")
                print("сгенерированная строка в виде бит: "+str(bit_string))
                print("строка с измененным битом:         "+str(random_change_one_bit(str_to_bin(string))))
                print("")
                cipher_text= encrypt_OFB(string, key)
                print("шифр текст:               "+str(cipher_text))
                string+=generate_random_string(8)
                cipher_text_add_block= encrypt_OFB(string, key)
                print("шифр текст с блоком:      "+str(cipher_text_add_block))
                string=string[:-8]
                cipher_text_rm_block= encrypt_OFB(string, key)
                print("шифр текст без блока:     "+str(cipher_text_rm_block))
                string=string[:-8]
                string+=generate_random_string(8)
                cipher_text_ch_block= encrypt_OFB(string, key)
                print("шифр текст с изм. блоком: "+str(cipher_text_ch_block))
                os.system("PAUSE")
            case 8:
                os.system("cls")
                string=generate_random_string(32)
                bit_string = str_to_bin(string)
                key = keygen(16)
                #print(key.decode('UTF-8'))
                print("сгенерированная строка: "+string)
                print("сгенерированный ключ (bit):   "+ str(byte_to_bin(key)))
                print("сгенерированный ключ (byte):   "+ str(repr(key)[12:-2]))
                print("")
                print("сгенерированная строка в виде бит: "+str(bit_string))
                print("строка с измененным битом:         "+str(random_change_one_bit(str_to_bin(string))))
                print("")
                cipher_text= encrypt_ECB(string, key)
                print("шифр текст:               "+str(cipher_text))
                string+=generate_random_string(8)
                cipher_text_add_block= encrypt_ECB(string, key)
                print("шифр текст с блоком:      "+str(cipher_text_add_block))
                string=string[:-8]
                cipher_text_rm_block= encrypt_ECB(string, key)
                print("шифр текст без блока:     "+str(cipher_text_rm_block))
                string=string[:-8]
                string+=generate_random_string(8)
                cipher_text_ch_block= encrypt_ECB(string, key)
                print("шифр текст с изм. блоком: "+str(cipher_text_ch_block))
                os.system("PAUSE")
            case 0:
                break
    except Exception:
        print("wrong input")

